package com.example.advancedtodoapplication.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.advancedtodoapplication.data.TodoItem

@Database(
    entities = [TodoItem::class],
    version = 1
)
@TypeConverters(Converter::class)
abstract class TodoDatabase : RoomDatabase() {

    abstract fun getTodoDao(): ToDoDao
}