package com.example.advancedtodoapplication.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.advancedtodoapplication.data.TodoItem
import kotlinx.coroutines.flow.Flow

@Dao
interface ToDoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTodo(todo: TodoItem)

    @Delete
    suspend fun deleteTodo(todo: TodoItem)

    @Query("SELECT * FROM todo_table")
    fun getAllTodo(): Flow<List<TodoItem>>

    @Query("DELETE FROM todo_table")
    suspend fun deleteAll()

    @Update
    suspend fun updateTodo(todo: TodoItem)

    @Query("SELECT * FROM todo_table ORDER BY CASE WHEN priority LIKE 'H%' THEN 1 WHEN priority LIKE 'M%' THEN 2 WHEN priority LIKE 'L%' THEN 3 END")
    fun sortByHighPriority():LiveData<List<TodoItem>>

    @Query("SELECT * FROM todo_table ORDER BY CASE WHEN priority LIKE 'L%' THEN 1 WHEN priority LIKE 'M%' THEN 2 WHEN priority LIKE 'H%' THEN 3 END")
    fun sortByLowPriority():LiveData<List<TodoItem>>

    @Query("SELECT * FROM todo_table WHERE title LIKE :searchQuery")
    fun searchDatabase(searchQuery:String) : Flow<List<TodoItem>>
}