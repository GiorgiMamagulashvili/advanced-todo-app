package com.example.advancedtodoapplication.di

import android.content.Context
import androidx.room.Room
import com.example.advancedtodoapplication.data.db.TodoDatabase
import com.example.advancedtodoapplication.util.Constant.DATABASE_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    fun provideDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        TodoDatabase::class.java,
        DATABASE_NAME
    ).build()

    @Provides
    fun provideDao(db: TodoDatabase) = db.getTodoDao()
}