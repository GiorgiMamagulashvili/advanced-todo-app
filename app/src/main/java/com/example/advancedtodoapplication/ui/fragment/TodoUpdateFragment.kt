package com.example.advancedtodoapplication.ui.fragment

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.advancedtodoapplication.R
import com.example.advancedtodoapplication.data.TodoItem
import com.example.advancedtodoapplication.databinding.FragmentUpdateBinding
import com.example.advancedtodoapplication.main.FunctionalViewModel
import com.example.advancedtodoapplication.main.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TodoUpdateFragment : Fragment(R.layout.fragment_update) {

    private var _binding: FragmentUpdateBinding? = null
    private val binding: FragmentUpdateBinding get() = _binding!!

    val args: TodoUpdateFragmentArgs by navArgs()
    private val funViewModel: FunctionalViewModel by viewModels()
    private val mainViewModel: MainViewModel by viewModels()

    override fun onResume() {
        super.onResume()
        val dropDownPriorities = resources.getStringArray(R.array.priority)
        val arrayAdapter =
            ArrayAdapter(requireContext(), R.layout.dropdown_item, dropDownPriorities)
        binding.spinner.setAdapter(arrayAdapter)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentUpdateBinding.bind(view)

        setArguments()

        binding.fab.setOnClickListener {
            updateTodo()
        }
    }


    private fun updateTodo() {
        val title = binding.titleEt.text.toString()
        val body = binding.descriptionEt.text.toString()
        val priority = binding.spinner.text.toString()

        val validation = funViewModel.verifyUserInputs(title, body)
        if (validation) {
            val item =
                TodoItem(args.TodoItem.id, title, funViewModel.validationPriority(priority), body)
            mainViewModel.updateTodo(item)
            Toast.makeText(requireContext(), "updated!", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_todoUpdateFragment_to_mainFragment)
        }

    }
    private fun setArguments(){
        binding.titleEt.setText(args.TodoItem.title)
        binding.descriptionEt.setText(args.TodoItem.description)
        binding.spinner.setText(args.TodoItem.priority.toString())
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}