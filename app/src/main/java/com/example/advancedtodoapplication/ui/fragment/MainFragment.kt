package com.example.advancedtodoapplication.ui.fragment

import android.app.AlertDialog
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.advancedtodoapplication.R
import com.example.advancedtodoapplication.databinding.FragmentMainBinding
import com.example.advancedtodoapplication.main.MainViewModel
import com.example.advancedtodoapplication.ui.TodoListAdapter
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator

@AndroidEntryPoint

class MainFragment : Fragment(R.layout.fragment_main), SearchView.OnQueryTextListener {

    private var _binding: FragmentMainBinding? = null
    private val binding: FragmentMainBinding get() = _binding!!

    private val mAdapter: TodoListAdapter by lazy { TodoListAdapter() }
    private val viewModel: MainViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentMainBinding.bind(view)

        setRecycleView()
        swipeDel()
        binding.fab.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_listAddFragment)
        }

        viewModel.getAllTodo.observe(viewLifecycleOwner, Observer { data ->
            mAdapter.differ.submitList(data)
        })
        setHasOptionsMenu(true)

    }

    private fun swipeDel() {
        val itemTouchHelper = object : ItemTouchHelper.SimpleCallback(
            ItemTouchHelper.UP or ItemTouchHelper.DOWN,
            ItemTouchHelper.RIGHT or ItemTouchHelper.LEFT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return true
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val todo = mAdapter.differ.currentList[position]
                viewModel.deleteTodo(todo)
                Snackbar.make(view!!, "Successfully Deleted", Snackbar.LENGTH_LONG).apply {
                    setAction("Undo") {
                        viewModel.insertTodo(todo)
                    }.show()
                }
            }

        }
        ItemTouchHelper(itemTouchHelper).apply {
            attachToRecyclerView(binding.rvList)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbar_menu, menu)

        val search = menu.findItem(R.id.menu_search)
        val searchView = search.actionView as? SearchView
        searchView?.isSubmitButtonEnabled = true
        searchView?.setOnQueryTextListener(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_delete_all -> deleteAll()
            R.id.menu_sort_high_priority -> {
                viewModel.sortByHighPriority.observe(viewLifecycleOwner, Observer { data ->
                    mAdapter.differ.submitList(data)
                })
            }
            R.id.menu_sort_low_priority -> {
                viewModel.sortByLowPriority.observe(viewLifecycleOwner, Observer { data ->
                    mAdapter.differ.submitList(data)
                })
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteAll() {
        val builder = AlertDialog.Builder(requireContext())

        builder.apply {
            setPositiveButton("Yes") { _, _ ->
                viewModel.deleteAll()
                Toast.makeText(
                    requireContext(),
                    "Successfully deleted all todos",
                    Toast.LENGTH_SHORT
                ).show()
            }
            setNegativeButton("No") { _, _ -> }
            setTitle("Delete Everything")
            setMessage("Do u wanna delete everything?")
            create().show()
        }
    }


    private fun setRecycleView() {
        binding.rvList.apply {
            layoutManager =
                GridLayoutManager(requireContext(), 2, GridLayoutManager.VERTICAL, false)
            adapter = mAdapter
            itemAnimator = SlideInUpAnimator().apply {
                addDuration = 300
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        if (query != null) {
            searchDatabase(query)
        }
        return true
    }

    override fun onQueryTextChange(query: String?): Boolean {
        if (query != null) {
            searchDatabase(query)
        }
        return true
    }

    private fun searchDatabase(query: String) {
        val searchQuery = "%$query%"
        viewModel.searchDatabase(searchQuery).observe(viewLifecycleOwner, Observer { data ->
            data.let {
                mAdapter.differ.submitList(it)
            }
        })
    }
}