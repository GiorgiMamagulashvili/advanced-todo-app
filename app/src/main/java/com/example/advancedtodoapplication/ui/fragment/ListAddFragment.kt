package com.example.advancedtodoapplication.ui.fragment


import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController

import com.example.advancedtodoapplication.R
import com.example.advancedtodoapplication.data.TodoItem
import com.example.advancedtodoapplication.databinding.FragmentAddListBinding
import com.example.advancedtodoapplication.main.FunctionalViewModel
import com.example.advancedtodoapplication.main.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ListAddFragment : Fragment(R.layout.fragment_add_list) {

    private var _binding: FragmentAddListBinding? = null
    private val binding: FragmentAddListBinding get() = _binding!!

    private val funViewModel: FunctionalViewModel by viewModels()
    private val viewModel: MainViewModel by viewModels()

    override fun onResume() {
        super.onResume()
        val dropDownPriorities = resources.getStringArray(R.array.priority)
        val arrayAdapter =
            ArrayAdapter(requireContext(), R.layout.dropdown_item, dropDownPriorities)
        binding.spinner.setAdapter(arrayAdapter)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentAddListBinding.bind(view)

        binding.fab.setOnClickListener {
            insertData()
        }

    }

    private fun insertData() {
        val title = binding.titleEt.text.toString()
        val body = binding.descriptionEt.text.toString()
        val priority = binding.spinner.text.toString()

        val validation = funViewModel.verifyUserInputs(title, body)

        if (validation) {
            val data = TodoItem(
                0,
                title,
                funViewModel.validationPriority(priority),
                body
            )
            viewModel.insertTodo(data)
            Toast.makeText(requireContext(), "Successfully added todo", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_listAddFragment_to_mainFragment)
        }else{
            Toast.makeText(requireContext(), "Please fill all field!", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}