package com.example.advancedtodoapplication.ui

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.advancedtodoapplication.data.Priority
import com.example.advancedtodoapplication.data.TodoItem
import com.example.advancedtodoapplication.databinding.RowItemLayoutBinding
import com.example.advancedtodoapplication.ui.fragment.MainFragmentDirections

class TodoListAdapter : RecyclerView.Adapter<TodoListAdapter.TodoViewHolder>() {

    inner class TodoViewHolder(val binding: RowItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(todoItem: TodoItem) {
            binding.apply {
                titleTxt.text = todoItem.title
                descriptionTxt.text = todoItem.description
                when (todoItem.priority) {
                    Priority.High -> priorityIndicator.setCardBackgroundColor(Color.RED)
                    Priority.Medium -> priorityIndicator.setCardBackgroundColor(Color.YELLOW)
                    Priority.Low -> priorityIndicator.setCardBackgroundColor(Color.GREEN)
                }
                rowBackground.setOnClickListener {
                    val action = MainFragmentDirections.actionMainFragmentToTodoUpdateFragment(todoItem)
                    itemView.findNavController().navigate(action)
                }
            }
        }
    }
    private val diffUtilCallback = object :DiffUtil.ItemCallback<TodoItem>(){
        override fun areItemsTheSame(oldItem: TodoItem, newItem: TodoItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: TodoItem, newItem: TodoItem): Boolean {
            return oldItem == newItem
        }

    }
    val differ = AsyncListDiffer(this,diffUtilCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        return TodoViewHolder(
            RowItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        val currentItem = differ.currentList[position]
        if (currentItem != null){
            holder.bind(currentItem)
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }
}