package com.example.advancedtodoapplication.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.advancedtodoapplication.data.TodoItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: MainRepository
) : ViewModel() {

    val getAllTodo = repository.getAllTodo().asLiveData()
    val sortByHighPriority:LiveData<List<TodoItem>> = repository.sortByHighPriority
    val sortByLowPriority:LiveData<List<TodoItem>> = repository.sortByLowPriority

    fun deleteTodo(todoItem: TodoItem) = viewModelScope.launch {
        repository.deleteTodo(todoItem)
    }
    fun insertTodo(todoItem: TodoItem) = viewModelScope.launch {
        repository.insertTodo(todoItem)
    }

    fun deleteAll() = viewModelScope.launch {
        repository.deleteAllTodo()
    }
    fun updateTodo(todoItem: TodoItem) = viewModelScope.launch {
        repository.updateTodo(todoItem)
    }

    fun  searchDatabase(searchQuery:String):LiveData<List<TodoItem>> = repository.searchDatabase(searchQuery).asLiveData()

}