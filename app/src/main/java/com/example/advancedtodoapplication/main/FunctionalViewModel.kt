package com.example.advancedtodoapplication.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.advancedtodoapplication.data.Priority

class FunctionalViewModel(application: Application) : AndroidViewModel(application) {

    fun verifyUserInputs(title: String, body: String): Boolean {
        return !(title.isEmpty() || body.isEmpty())
    }

    fun validationPriority(priority: String): Priority {
        return when (priority) {
            "High Priority" -> {
                Priority.High
            }
            "Medium Priority" -> {
                Priority.Medium
            }
            "Low Priority" -> {
                Priority.Low
            }
            else -> {
                Priority.Low
            }
        }
    }
}