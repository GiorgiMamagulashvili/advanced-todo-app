package com.example.advancedtodoapplication.main

import androidx.lifecycle.LiveData
import com.example.advancedtodoapplication.data.TodoItem
import com.example.advancedtodoapplication.data.db.ToDoDao
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class MainRepository @Inject constructor(
    private val dao: ToDoDao,

    ) {
    suspend fun insertTodo(todo: TodoItem) = dao.insertTodo(todo)

    suspend fun deleteTodo(todo: TodoItem) = dao.deleteTodo(todo)

    fun getAllTodo() = dao.getAllTodo()

    suspend fun deleteAllTodo() = dao.deleteAll()

    suspend fun updateTodo(todo: TodoItem) = dao.updateTodo(todo)

    val sortByHighPriority: LiveData<List<TodoItem>> = dao.sortByHighPriority()

    val sortByLowPriority :LiveData<List<TodoItem>> = dao.sortByLowPriority()

    fun searchDatabase(searchQuery:String) = dao.searchDatabase(searchQuery)
}